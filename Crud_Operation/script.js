var markRow = null

function onFormSubmit()
{
    if(validate())
    {
        var data = readData();
        if(markRow==null)
            insertRecord(data);
        
        else
             updateRecord(data);
        
        resetForm();
    }
}

function readData()
{
    var data ={};
    data["fullName"] = document.getElementById("fullName").value;
    data["employeeNo"] = document.getElementById("employeeNo").value;
    data["salary"] = document.getElementById("salary").value;
    data["location"] = document.getElementById("location").value;
    return data;
}

function insertRecord(info)
{
    var table = document.getElementById("empList").getElementsByTagName('tbody')[0];
    var newRow = table.insertRow(table.length);
    
    cell1 = newRow.insertCell(0);
    cell1.innerHTML = info.fullName;

    cell2 = newRow.insertCell(1);
    cell2.innerHTML = info.employeeNo;

    cell3 = newRow.insertCell(2);
    cell3.innerHTML = info.salary;

    cell4 = newRow.insertCell(3);
    cell4.innerHTML = info.location;

    cell4 = newRow.insertCell(4);
    cell4.innerHTML = `<a onClick="onEdit(this)">Edit</a>
                        <a onClick="onDelete(this)">Delete</a>`;

}

function resetForm()
{
    document.getElementById("fullName").value = "";
    document.getElementById("employeeNo").value = "";
    document.getElementById("salary").value = "";
    document.getElementById("location").value = "";
    markRow = null;
}

function onEdit(td)
{
    markRow = td.parentElement.parentElement;
    document.getElementById("fullName").value = markRow.cells[0].innerHTML;
    document.getElementById("employeeNo").value = markRow.cells[1].innerHTML;
    document.getElementById("salary").value = markRow.cells[2].innerHTML;
    document.getElementById("location").value = markRow.cells[3].innerHTML; 

}

function updateRecord(data)
{
    markRow.cells[0].innerHTML = data.fullName;
    markRow.cells[1].innerHTML = data.employeeNo;
    markRow.cells[2].innerHTML = data.salary;
    markRow.cells[3].innerHTML = data.location;

}


function onDelete(td)
{
    if(confirm('Are you sure to delete this record ?'))
    {
        row = td.parentElement.parentElement;
        document.getElementById("empList").deleteRow(row.rowIndex);
        resetForm();

    }
}

function validate()
{
    isValid = true;
    if (document.getElementById("fullName").value == "")
    {
        isValid = false;
        document.getElementById("fullNameValidationError").classList.remove("hide");

    }
    else
    {
        isValid = true;
        if(!document.getElementById("fullNameValidationError").classList.contains("hide"))
        {
        document.getElementById("fullNameValidationError").classList.add("hide");
        }

    }
    return isValid;
}

